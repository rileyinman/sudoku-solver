use std::{fmt, io, ops, slice};
use std::fs::File;
use std::io::Read;
use std::str::FromStr;
use filtered_string;

/// Represents a sudoku board as a struct containing a 9x9 array.
pub struct Board {
    pub (self) data: [[u32; 9]; 9]
}

impl Board {
    /// Takes the path to a file and parses it as a Sudoku board.
    ///
    /// # Errors
    ///
    /// This function returns Err if the file is not found or cannot be parsed.
    ///
    /// # Example
    ///
    /// ```
    /// # use sudoku_solver::board::Board;
    /// let board = Board::from_file("sudoku-board.txt");
    /// assert!(board.is_ok());
    /// ```
    ///
    /// ```
    /// # use sudoku_solver::board::Board;
    /// let board = Board::from_file("not-a-file.txt");
    /// assert!(board.is_err());
    /// ```
    pub fn from_file(path: &str) -> Result<Board, io::Error> {
        // Turn file into string
        let mut file = File::open(path)?;
        let mut string = String::new();
        file.read_to_string(&mut string)?;

        // Return Result from the string conversion method
        Board::from_str(&string)
    }

    /// Takes the index of a row and returns that row.
    ///
    /// # Example
    /// ```
    /// # use std::str::FromStr;
    /// # use sudoku_solver::board::Board;
    /// let board = Board::from_str(r#"
    ///         5 3 _ | _ 7 _ | _ _ _
    ///         6 _ _ | 1 9 5 | _ _ _
    ///         _ 9 8 | _ _ _ | _ 6 _
    ///         ------+-------+------
    ///         8 _ _ | _ 6 _ | _ _ 3
    ///         4 _ _ | 8 _ 3 | _ _ 1
    ///         7 _ _ | _ 2 _ | _ _ 6
    ///         ------+-------+------
    ///         _ 6 _ | _ _ _ | 2 8 _
    ///         _ _ _ | 4 1 9 | _ _ 5
    ///         _ _ _ | _ 8 _ | _ 7 9
    /// "#).unwrap();
    ///
    /// let row: [u32; 9] = board.row(1);
    ///
    /// assert_eq!(row, [6, 0, 0, 1, 9, 5, 0, 0, 0]);
    /// ```
    pub fn row(&self, y: usize) -> [u32; 9] {
        self.data[y]
    }

    /// Takes the index of a column and returns that column.
    ///
    /// # Example
    /// ```
    /// # use std::str::FromStr;
    /// # use sudoku_solver::board::Board;
    /// let board = Board::from_str(r#"
    ///         5 3 _ | _ 7 _ | _ _ _
    ///         6 _ _ | 1 9 5 | _ _ _
    ///         _ 9 8 | _ _ _ | _ 6 _
    ///         ------+-------+------
    ///         8 _ _ | _ 6 _ | _ _ 3
    ///         4 _ _ | 8 _ 3 | _ _ 1
    ///         7 _ _ | _ 2 _ | _ _ 6
    ///         ------+-------+------
    ///         _ 6 _ | _ _ _ | 2 8 _
    ///         _ _ _ | 4 1 9 | _ _ 5
    ///         _ _ _ | _ 8 _ | _ 7 9
    /// "#).unwrap();
    ///
    /// let column: [u32; 9] = board.column(4);
    ///
    /// assert_eq!(column, [7, 9, 0, 6, 0, 2, 0, 1, 8]);
    /// ```
    pub fn column(&self, x: usize) -> [u32; 9] {
        let mut column: [u32; 9] = [0; 9];

        for (i, row) in self.iter().enumerate() {
            column[i] = row[x];
        }

        column
    }

    /// Takes the x and y coordinates of a cell
    /// and returns an array representation of its 3x3 grid.
    ///
    /// # Example
    /// ```
    /// # use std::str::FromStr;
    /// # use sudoku_solver::board::Board;
    /// let board = Board::from_str(r#"
    ///         5 3 _ | _ 7 _ | _ _ _
    ///         6 _ _ | 1 9 5 | _ _ _
    ///         _ 9 8 | _ _ _ | _ 6 _
    ///         ------+-------+------
    ///         8 _ _ | _ 6 _ | _ _ 3
    ///         4 _ _ | 8 _ 3 | _ _ 1
    ///         7 _ _ | _ 2 _ | _ _ 6
    ///         ------+-------+------
    ///         _ 6 _ | _ _ _ | 2 8 _
    ///         _ _ _ | 4 1 9 | _ _ 5
    ///         _ _ _ | _ 8 _ | _ 7 9
    /// "#).unwrap();
    ///
    /// let grid: [u32; 9] = board.grid_of(1, 4);
    ///
    /// assert_eq!(grid, [0, 7, 0, 1, 9, 5, 0, 0, 0]);
    /// ```
    pub fn grid_of(&self, row: usize, column: usize) -> [u32; 9] {
        let mut grid: [u32; 9] = [0; 9];

        let start_row = (row / 3) * 3;
        let start_column = (column / 3) * 3;

        let mut grid_index = 0;
        for i in start_row..start_row+3 {
            for j in start_column..start_column+3 {
                grid[grid_index] = self.data[i][j];
                grid_index += 1;
            }
        }

        grid
    }

    /// Returns a reference to the internal data
    pub fn as_arr(&self) -> &[[u32; 9]; 9] {
        &self.data
    }

    /// Iterates over the Board's array data.
    pub fn iter(&self) -> slice::Iter<[u32; 9]> {
        self.data.iter()
    }

    /// Iterates mutably over the Board's array data.
    pub fn iter_mut(&mut self) -> slice::IterMut<[u32; 9]> {
        self.data.iter_mut()
    }

    /// Returns true if the Board contains the given number, false otherwise.
    pub fn contains(&self, num: u32) -> bool {
        for row in self.iter() {
            for &cell in row.iter() {
                if cell == num {
                    return true
                }
            }
        }

        false
    }
}

impl FromStr for Board {
    type Err = io::Error;

    /// Takes a string and parses it as a Sudoku board.
    ///
    /// The board can be in any format
    /// as long as the cells are in order.
    /// Delimiting characters are ignored
    /// and blank spots can be represented by 0, ., or _.
    ///
    /// # Example
    /// ```
    /// # use std::str::FromStr;
    /// # use sudoku_solver::board::Board;
    /// let board = Board::from_str(r#"
    ///         5 3 _ | _ 7 _ | _ _ _
    ///         6 _ _ | 1 9 5 | _ _ _
    ///         _ 9 8 | _ _ _ | _ 6 _
    ///         ------+-------+------
    ///         8 _ _ | _ 6 _ | _ _ 3
    ///         4 _ _ | 8 _ 3 | _ _ 1
    ///         7 _ _ | _ 2 _ | _ _ 6
    ///         ------+-------+------
    ///         _ 6 _ | _ _ _ | 2 8 _
    ///         _ _ _ | 4 1 9 | _ _ 5
    ///         _ _ _ | _ 8 _ | _ 7 9
    /// "#);
    ///
    /// assert!(board.is_ok());
    /// ```
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut board: [[u32; 9]; 9] = [[0; 9]; 9];

        // Remove all characters except numbers, _, and .
        let s = filtered_string(s);
        assert_eq!(s.len(), 81, "Board has incorrect number of cells");

        // Loop over characters in string and map to digits in board array
        for (i, row) in board.iter_mut().enumerate() {
            for (j, cell) in row.iter_mut().enumerate() {
                // Index over characters in string, panic if not enough chararacters
                let ch = s.as_bytes()[i*9 + j];
                match ch {
                    b'_' | b'.' => *cell = 0,
                    b'0' ..= b'9' => *cell = (ch - b'0').into(), // Convert char into u32
                    _ => {}
                }
            }
        }

        // Return success with a new Sudoku object
        Ok(Board { data: board })
    }
}

impl fmt::Display for Board {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{} {} {} | {} {} {} | {} {} {}\n\
                     {} {} {} | {} {} {} | {} {} {}\n\
                     {} {} {} | {} {} {} | {} {} {}\n\
                     ------+-------+------\n\
                     {} {} {} | {} {} {} | {} {} {}\n\
                     {} {} {} | {} {} {} | {} {} {}\n\
                     {} {} {} | {} {} {} | {} {} {}\n\
                     ------+-------+------\n\
                     {} {} {} | {} {} {} | {} {} {}\n\
                     {} {} {} | {} {} {} | {} {} {}\n\
                     {} {} {} | {} {} {} | {} {} {}",
        self[0][0], self[0][1], self[0][2], self[0][3], self[0][4], self[0][5], self[0][6], self[0][7], self[0][8],
        self[1][0], self[1][1], self[1][2], self[1][3], self[1][4], self[1][5], self[1][6], self[1][7], self[1][8],
        self[2][0], self[2][1], self[2][2], self[2][3], self[2][4], self[2][5], self[2][6], self[2][7], self[2][8],
        self[3][0], self[3][1], self[3][2], self[3][3], self[3][4], self[3][5], self[3][6], self[3][7], self[3][8],
        self[4][0], self[4][1], self[4][2], self[4][3], self[4][4], self[4][5], self[4][6], self[4][7], self[4][8],
        self[5][0], self[5][1], self[5][2], self[5][3], self[5][4], self[5][5], self[5][6], self[5][7], self[5][8],
        self[6][0], self[6][1], self[6][2], self[6][3], self[6][4], self[6][5], self[6][6], self[6][7], self[6][8],
        self[7][0], self[7][1], self[7][2], self[7][3], self[7][4], self[7][5], self[7][6], self[7][7], self[7][8],
        self[8][0], self[8][1], self[8][2], self[8][3], self[8][4], self[8][5], self[8][6], self[8][7], self[8][8])
    }
}

/// Overloads [] to allow indexing on a Board
impl ops::Index<usize> for Board {
    type Output = [u32; 9];

    fn index(&self, index: usize) -> &Self::Output {
        &self.data[index]
    }
}

/// Mutable indexing
impl ops::IndexMut<usize> for Board {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.data[index]
    }
}
