pub mod board;

use board::Board;

/// Attempts to solve the given board.
/// # Example
/// ```
/// # use std::str::FromStr;
/// # use sudoku_solver::board::Board;
/// # use sudoku_solver::solve;
/// let mut board = Board::from_str(r#"
///         5 3 _ | _ 7 _ | _ _ _
///         6 _ _ | 1 9 5 | _ _ _
///         _ 9 8 | _ _ _ | _ 6 _
///         ------+-------+------
///         8 _ _ | _ 6 _ | _ _ 3
///         4 _ _ | 8 _ 3 | _ _ 1
///         7 _ _ | _ 2 _ | _ _ 6
///         ------+-------+------
///         _ 6 _ | _ _ _ | 2 8 _
///         _ _ _ | 4 1 9 | _ _ 5
///         _ _ _ | _ 8 _ | _ 7 9
/// "#).unwrap();
///
/// solve(&mut board);
///
/// assert_eq!(board.as_arr(), &[
///     [5, 3, 4, 6, 7, 8, 9, 1, 2],
///     [6, 7, 2, 1, 9, 5, 3, 4, 8],
///     [1, 9, 8, 3, 4, 2, 5, 6, 7],
///     [8, 5, 9, 7, 6, 1, 4, 2, 3],
///     [4, 2, 6, 8, 5, 3, 7, 9, 1],
///     [7, 1, 3, 9, 2, 4, 8, 5, 6],
///     [9, 6, 1, 5, 3, 7, 2, 8, 4],
///     [2, 8, 7, 4, 1, 9, 6, 3, 5],
///     [3, 4, 5, 2, 8, 6, 1, 7, 9]
/// ]);
/// ```
pub fn solve(mut board: &mut Board) {
    while board.contains(0) {
        for i in 0..9 {
            for j in 0..9 {
                if board[i][j] == 0 {
                    update_cell(&mut board, i, j);
                }
            }
        }
    }
}


/// Takes a string and returns its filtered characters with no newlines.
///
/// # Example
/// ```
/// # use sudoku_solver::filtered_string;
/// let string = "abcd1\nefgh2\nijkl3\nmnop4";
///
/// let new_string = filtered_string(string);
///
/// assert_eq!(new_string, "1234");
/// ```
pub fn filtered_string(string: &str) -> String {
    let mut new_string: String = String::new();

    for line in string.lines() {
        new_string += line;
    }

    new_string.chars().filter(|&c| "0123456789_.".contains(c)).collect()
}

/// Attempts to solve the cell at (x, y) in board.
///
/// # Example
/// ```
/// # use std::str::FromStr;
/// # use sudoku_solver::board::Board;
/// # use sudoku_solver::update_cell;
/// let mut board = Board::from_str(r#"
///         5 3 _ | _ 7 _ | _ _ _
///         6 _ _ | 1 9 5 | _ _ _
///         _ 9 8 | _ _ _ | _ 6 _
///         ------+-------+------
///         8 _ _ | _ 6 _ | _ _ 3
///         4 _ _ | 8 _ 3 | _ _ 1
///         7 _ _ | _ 2 _ | _ _ 6
///         ------+-------+------
///         _ 6 _ | _ _ _ | 2 8 _
///         _ _ _ | 4 1 9 | _ _ 5
///         _ _ _ | _ 8 _ | _ 7 9
/// "#).unwrap();
///
/// update_cell(&mut board, 4, 4);
///
/// assert_eq!(board[4][4], 5);
/// ```
pub fn update_cell(board: &mut Board, y: usize, x: usize) {
    let mut possibilities = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

    let row = board.row(y).to_vec();
    possibilities.retain(|num| !row.contains(num));

    let column = board.column(x).to_vec();
    possibilities.retain(|num| !column.contains(num));

    let grid = board.grid_of(y, x).to_vec();
    possibilities.retain(|num| !grid.contains(num));

    if possibilities.len() == 1 {
        board[y][x] = possibilities[0];
    }
}
